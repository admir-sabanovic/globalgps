# GGPS

# Install
`npm install`

# Run

## Development
`npm run dev`

## Production
`npm start`

Runs from `dist` folder, requires a build first.

# Build
`npm run build:prod`