import {enableProdMode} from '@angular/core';
import {HTTP_PROVIDERS} from '@angular/http';
import {bootstrap} from '@angular/platform-browser-dynamic';
import {ROUTER_PROVIDERS} from '@angular/router';
import {MainComponent} from './app/main.component';

if ('development' !== process.env.ENV) {
    enableProdMode();
}


bootstrap(MainComponent, [ROUTER_PROVIDERS, HTTP_PROVIDERS]);
