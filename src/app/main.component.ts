import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Routes} from '@angular/router';
import {SidebarComponent} from './common/sidebar/sidebar.component.ts';
import {HeaderComponent} from './common/header/header.component.ts';
import {MaterializeDirective} from 'angular2-materialize';
import {LocationMapComponent} from './location-map/location-map.component.ts';
import {LineMapComponent} from './line-map/line-map.component';
import '../assets/styles/main.css';

@Routes([
    {path: '/', component: LocationMapComponent},
    {path: '/line', component: LineMapComponent},
])
@Component({
    selector: 'ggps-app',
    templateUrl: 'app/main.component.html',
    directives: [ROUTER_DIRECTIVES, MaterializeDirective, SidebarComponent, HeaderComponent],

})
export class MainComponent {

    constructor() {
    }
}
