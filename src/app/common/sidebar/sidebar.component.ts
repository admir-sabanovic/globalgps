import {Component} from '@angular/core';
import {Location} from '@angular/common';
import {MaterializeDirective} from 'angular2-materialize';
import {ROUTER_DIRECTIVES} from '@angular/router';

@Component({
    selector: 'adminSidebar',
    templateUrl: 'app/common/sidebar/sidebar.component.html',
    directives: [MaterializeDirective, ROUTER_DIRECTIVES],
})
export class SidebarComponent {
    constructor(public location:Location) {

    }

    isActiveLink(path:string) {
        return this.location.path() === path;
    }
}

