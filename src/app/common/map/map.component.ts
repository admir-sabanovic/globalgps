import {Component, OnInit} from '@angular/core';

var L = require('leaflet');
require('leaflet_css');
require('leaflet_marker');
require('leaflet_marker_2x');
require('leaflet_marker_shadow');

@Component({
    selector: 'map',
    templateUrl: 'app/common/map/map.component.html',
})
export class MapComponent implements OnInit {
    private map:any;

    constructor() {
    }

    ngOnInit() {
        L.Icon.Default.imagePath = 'leaflet/images';
        this.map = L.map('mapid').setView([43.856259, 18.413076], 13);
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 18,
            id: 'criticalbh.0hnbff7g',
            accessToken: 'pk.eyJ1IjoiY3JpdGljYWxiaCIsImEiOiJjaXEyczcwcGkwMDV1aTJuZDlza3U3dnVqIn0.QsjcpqayjGUKDKcPuWFDQQ'
        }).addTo(this.map);
    }

    drawLine(latlng:Array<any>) {
        L.polyline(latlng).addTo(this.map);
    }

    addMarker(latlng:Array<any>) {
        L.marker(latlng).addTo(this.map);
    }

}
