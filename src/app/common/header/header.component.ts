import {Component} from '@angular/core';
import {MaterializeDirective} from 'angular2-materialize';


@Component({
    selector: 'adminHeader',
    templateUrl: 'app/common/header/header.component.html',
    directives: [MaterializeDirective]
})
export class HeaderComponent {

    sideMenuBig = true;
    showProfileMenu = false;
    user = null;

    profilePicClick() {
        this.showProfileMenu = !this.showProfileMenu;
    }

    toggleMenu() {
        this.sideMenuBig = !this.sideMenuBig;
    }
}
