import {Component, AfterViewInit, ViewChild} from '@angular/core';
import {MapComponent} from '../common/map/map.component';


@Component({
    templateUrl: 'app/location-map/location-map.component.html',
    directives: [MapComponent]
})
export class LocationMapComponent implements AfterViewInit {
    @ViewChild('locationMap') map: MapComponent;

    constructor() {
    }

    ngAfterViewInit() {
        this.map.addMarker([43.856259, 18.413076]);
    }
}
