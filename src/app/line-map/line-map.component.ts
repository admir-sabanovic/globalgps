import {Component, AfterViewInit, ViewChild} from '@angular/core';
import {MapComponent} from '../common/map/map.component';

@Component({
    templateUrl: 'app/line-map/line-map.component.html',
    directives: [MapComponent]
})
export class LineMapComponent implements AfterViewInit {
    @ViewChild('lineMap') map: MapComponent;

    constructor() {
    }

    ngAfterViewInit() {
        this.map.drawLine([[43.856259, 18.413076], [43.846068, 18.347318]]);
    }
}
