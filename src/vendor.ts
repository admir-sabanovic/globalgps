declare var require;

import 'es6-shim';


import 'reflect-metadata';
require('zone.js/dist/zone');
require('zone.js/dist/long-stack-trace-zone'); // for development only - not needed

// Materialize
import 'angular2-materialize';

// RxJS
import 'rxjs';
